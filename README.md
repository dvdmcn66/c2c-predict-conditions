# Snow Quality and Outings Prediction Project

This project aims to test the prediction of snow quality (for skiing or mountaineering) and general outing quality based on the past 30 days of weather data. The data is extracted and scraped from the Camptocamp website, and the weather data is retrieved via an API.
It was developed by David Micouin--Jorda.

## Description

- **Data Scraping**: Data from Camptocamp is scraped using a method from another project by Julien Eyzat.
- **Weather Data**: Weather data is obtained through an API.
- **Analysis and Prediction**: Tests were conducted in Jupyter Lab before implementing the complete pipeline. (to be improved...)

## Installation

1. Clone the project repository.
2. Install dependencies with the following command:
    ```bash
    pip install -r requirements.txt
    ```

## Usage

### Quick Launch

To quickly test the project with limited data, use the `quick_launch.py` script:
```bash
python quick_launch.py
```

### Full Launch

For a complete execution, launch the following scripts successively:

1. Data Extraction:
    ```bash
    python 1_extract.py
    ```

2. Data Transformation:
    ```bash
    python 2_transform_data.py
    ```

3. Prediction Testing:
    ```bash
    python 3_test_prediction.py
    ```
### Incremental Update of Weather Data

As the weather API has limitations on daily requests in its free mode, the script is designed to be incremental. You can relaunch it daily using:
```bash
python launch_meteo_only.py
```
