
import requests
import tqdm
import os
import json

def scrap_outings(docs_uri_list):
                    
    doc_uri = "https://api.camptocamp.org/outings/{doc_id}?cook=fr"
    doc_type = "outings"
    if not os.path.isdir("../data"):
        os.mkdir("../data")
    if not os.path.isdir(os.path.join("../data", doc_type)):
        os.mkdir(os.path.join("../data", doc_type))
    downloaded_ids = set([os.path.splitext(file_name)[0] for file_name in os.listdir(f"../data/{doc_type}")])
    r = requests.get(docs_uri_list[0].format(i=0))
    rjson = r.json()
    total_doc = rjson["total"]
    doc_ids = []
    for docs_uri in docs_uri_list:
        for i in tqdm.tqdm(range(0, total_doc+1, 100)):
            try:
                r = requests.get(docs_uri.format(i=i))
                rjson = r.json()
                doc_ids += [ doc["document_id"] for doc in rjson["documents"] if "documents" in rjson]
                if str(doc_ids[-1]) in downloaded_ids:
                    continue
            except:
                print("bug")
                break
    if not os.path.isdir("../data"):
        os.mkdir("../data")
    if not os.path.isdir(os.path.join("../data", doc_type)):
        os.mkdir(os.path.join("../data", doc_type))
    for doc_id in tqdm.tqdm(doc_ids):
        if not str(doc_id) in downloaded_ids:
            r = requests.get(doc_uri.format(doc_id=doc_id))
            rjson = r.json()
            with open(f"../data/{doc_type}/{doc_id}.json", 'w') as f:
                json.dump(rjson, f)