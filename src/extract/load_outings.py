import json
import pandas as pd
import os
import tqdm

def load_outings():
    outing_direct_keys = [
        "document_id",
        "quality",
        "access_condition",
        "avalanche_signs",
        "condition_rating",
        "date_end",
        "date_start",
        "elevation_access",
        "elevation_down_snow",
        "elevation_max",
        "elevation_min",
        "elevation_up_snow",
        "frequentation",
        "glacier_rating",
        "height_diff_down",
        "height_diff_up",
        "hut_status",
        "length_total",
        "lift_status",
        "partial_trip",
        "participant_count",
        "public_transport",
        "hiking_rating",
        "snow_quality",
        "snow_quantity",
        "global_rating",
        "height_diff_difficulties",
        "engagement_rating",
        "ski_rating",
        "labande_global_rating"
    ]
    cooked_keys = [
        "lang",
        "title",
        "description",
        "summary",
        "access_comment",
        "avalanches",
        "conditions",
        "conditions_levels",
        "hut_comment",
        "participants",
        "route_description",
        "timing",
        "weather"
    ]
    outings = []
    for outing_file in tqdm.tqdm(os.listdir("../data/outings")):
        # Open outings
        outing_path = os.path.join("../data/outings", outing_file)
        with open(outing_path) as f:
            json_outing = json.load(f)
        outing = {}
        # General infos
        for outing_direct_key in outing_direct_keys:
            outing[outing_direct_key] = json_outing.get(outing_direct_key, None)
        # Activities
        for activity in json_outing["activities"]:
            outing[f"activity_{activity}"] = True
        # Position
        outing["geom"] = json_outing["geometry"]["geom"]
        # Associated routes
        outing["associated_route_ids"] = []
        for associated_route in json_outing["associations"]["routes"]:
            outing["associated_route_ids"].append(associated_route["document_id"])
        # Associated users
        outing["associated_forum_usernames"] = []
        for associated_user in json_outing["associations"]["users"]:
            outing["associated_forum_usernames"].append(associated_user["forum_username"])
        # Postition
        for area in json_outing["areas"]:
            key = area["area_type"]
            value = area["locales"][0]["title"]
            outing[key] = value
        # Text
        for cooked_key in cooked_keys:
            outing[f"cooked_{cooked_key}"] = json_outing["cooked"].get(cooked_key, None)
        # Add outing link
        outing["link"] = f"https://www.camptocamp.org/outings/{outing['document_id']}"
        # Save
        outings.append(outing)
        # break
    df = pd.DataFrame(outings)
    if not os.path.isdir(os.path.join("../data", "raw")):
        os.mkdir(os.path.join("../data", "raw"))
    df.to_csv("../data/raw/outings.csv", index=False)