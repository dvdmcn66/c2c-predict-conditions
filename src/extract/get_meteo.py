import openmeteo_requests
from datetime import datetime, timedelta
import requests_cache
import pandas as pd
from retry_requests import retry
import pandas as pd
import numpy as np
import json
from pyproj import Transformer

def call_meteo(end_date, lat, lng):
    # Setup the Open-Meteo API client with cache and retry on error
    cache_session = requests_cache.CachedSession('.cache', expire_after = -1)
    retry_session = retry(cache_session, retries = 5, backoff_factor = 0.2)
    openmeteo = openmeteo_requests.Client(session = retry_session)

    
    end_date_format = datetime.strptime(end_date, "%Y-%m-%d")
    
    # Calculer la date 20 jours avant
    start_date = end_date_format - timedelta(days=30)
    
    # Convertir la date en format str
    start_date_str = start_date.strftime("%Y-%m-%d")
    # Make sure all required weather variables are listed here
    # The order of variables in hourly or daily is important to assign them correctly below
    url = "https://archive-api.open-meteo.com/v1/archive"
    params = {
    	"latitude": lat,
    	"longitude": lng,
    	"start_date": start_date_str,
    	"end_date": end_date,
    	"daily": ["temperature_2m_max", "temperature_2m_min", "daylight_duration", "rain_sum", "snowfall_sum"],
    	"timeformat": "unixtime",
    	"timezone": "Europe/London"
    }
    responses = openmeteo.weather_api(url, params=params)
    
    # Process first location. Add a for-loop for multiple locations or weather models
    response = responses[0]
    #print(f"Coordinates {response.Latitude()}°N {response.Longitude()}°E")
    #print(f"Elevation {response.Elevation()} m asl")
    #print(f"Timezone {response.Timezone()} {response.TimezoneAbbreviation()}")
    #print(f"Timezone difference to GMT+0 {response.UtcOffsetSeconds()} s")
    #print(responses)
    
    # Process daily data. The order of variables needs to be the same as requested.
    daily = response.Daily()
    daily_temperature_2m_max = daily.Variables(0).ValuesAsNumpy()
    daily_temperature_2m_min = daily.Variables(1).ValuesAsNumpy()
    daily_daylight_duration = daily.Variables(2).ValuesAsNumpy()
    daily_rain_sum = daily.Variables(3).ValuesAsNumpy()
    daily_snowfall_sum = daily.Variables(4).ValuesAsNumpy()
    
    daily_data = {"date": pd.date_range(
    	start = pd.to_datetime(daily.Time(), unit = "s", utc = True),
    	end = pd.to_datetime(daily.TimeEnd(), unit = "s", utc = True),
    	freq = pd.Timedelta(seconds = daily.Interval()),
    	inclusive = "left"
    )}
    daily_data["temperature_2m_max"] = daily_temperature_2m_max
    daily_data["temperature_2m_min"] = daily_temperature_2m_min
    daily_data["daylight_duration"] = daily_daylight_duration
    daily_data["rain_sum"] = daily_rain_sum
    daily_data["snowfall_sum"] = daily_snowfall_sum
    
    daily_dataframe = pd.DataFrame(data = daily_data)
    return daily_dataframe


def add_col_number_day(meteo_data_copy):
    meteo_data_copy['date'] = pd.to_datetime(meteo_data_copy['date'])

    # Trouver la date minimale et maximale
    min_date = meteo_data_copy['date'].min()
    max_date = meteo_data_copy['date'].max()
    
    # Calculer la différence en jours
    num_days = (max_date - min_date).days
    
    # Créer une nouvelle colonne 'jour'
    meteo_data_copy['jour'] = (meteo_data_copy['date'] - min_date).dt.days
    
    # Renommer les valeurs de 'jour' pour commencer à partir de jour0
    meteo_data_copy['jour'] = meteo_data_copy['jour'].astype(str)
    
    # Afficher le DataFrame avec la colonne 'jour' ajoutée
    return meteo_data_copy

def get_meteo():
    input_df = pd.read_csv("../data/raw/outings.csv")
    df = input_df.copy()
    transformer = Transformer.from_crs(3857, 4326)
    hasToInit = False
    try:
        global_meteo_df = pd.read_csv("../data/raw/meteo_sortie.csv")
    except:
        global_meteo_df = pd.DataFrame()
        hasToInit = True
    first_time = True
    nb_errors_cumulative = 0
    for index, row in df.iterrows():
        try:
            #print(row["date_start"], row["geom"], row['document_id'])
            point = json.loads(row['geom'])['coordinates']
            gps_coord = transformer.transform(point[0], point[1])
            #print('gps', gps_coord)
            if not hasToInit and row['document_id'] in global_meteo_df['document_id'].unique():
                continue
            if first_time:
                first_time = False
                print(index)
            meteo_data = call_meteo(row["date_start"], gps_coord[0], gps_coord[1])
            meteo_data['document_id'] = row['document_id']
            if hasToInit:
                hasToInit = False
                global_meteo_df = meteo_data
            else:
                global_meteo_df = pd.concat([global_meteo_df, meteo_data], ignore_index=True)

            if index % 500 == 1:
                global_meteo_df.to_csv("../data/raw/meteo_sortie.csv", index=False)
                print(index)
            nb_errors_cumulative = 0
        except Exception as e:
            print('error', row['document_id'], e)
            nb_errors_cumulative += 1
            if nb_errors_cumulative > 20:
                break

    global_meteo_df.to_csv("../data/raw/meteo_sortie.csv", index=False)
