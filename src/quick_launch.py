from extract.scrap_outings import scrap_outings
from extract.load_outings import load_outings
from extract.get_meteo import get_meteo

if __name__ == '__main__':
    #Quick launch : Launch with only one uri to scrap
    # It allow to launch the complete pipeline (knowing meteo api has a daily limitation)

    docs_uri_list = ["https://api.camptocamp.org/outings?qa=medium,great&bbox=-267209,5098197,167915,5486644&act=skitouring&offset={i}&limit=100"]
    scrap_outings(docs_uri_list)
    load_outings()
    get_meteo()
    build_intermediate_meteo()
    build_intermediate_outings()
    build_standard_meteo()
    build_standard_outings()
    build_datamart()
    predict()