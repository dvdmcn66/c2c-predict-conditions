
from extract.get_meteo import get_meteo

if __name__ == '__main__':
    #get_meteo is incremental, we must launch several times because of the daily api request limit
    get_meteo()