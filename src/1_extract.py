from extract.scrap_outings import scrap_outings
from extract.load_outings import load_outings
from extract.get_meteo import get_meteo

if __name__ == '__main__':

    docs_uri_list = ["https://api.camptocamp.org/outings?qa=medium,great&bbox=-267209,5098197,167915,5486644&act=skitouring&offset={i}&limit=100",
                    "https://api.camptocamp.org/outings?qa=medium,great&bbox=32993,5102867,468117,5491315&act=skitouring&offset={i}&limit=100",
                    "https://api.camptocamp.org/outings?qa=medium,great&bbox=275967,5343923,711090,5732370&act=skitouring&offset={i}&limit=100",
                    "https://api.camptocamp.org/outings?qa=medium,great&bbox=719611,5548330,1154735,5936778&act=skitouring&offset={i}&limit=100",
                    "https://api.camptocamp.org/outings?qa=medium,great&bbox=-267209,5098197,167915,5486644&act=snow_ice_mixed&offset={i}&limit=100",
                    "https://api.camptocamp.org/outings?qa=medium,great&bbox=32993,5102867,468117,5491315&act=snow_ice_mixed&offset={i}&limit=100",
                    "https://api.camptocamp.org/outings?qa=medium,great&bbox=275967,5343923,711090,5732370&act=snow_ice_mixed&offset={i}&limit=100",
                    "https://api.camptocamp.org/outings?qa=medium,great&bbox=719611,5548330,1154735,5936778&act=snow_ice_mixed&offset={i}&limit=100"]
    scrap_outings(docs_uri_list)
    load_outings()
    get_meteo()