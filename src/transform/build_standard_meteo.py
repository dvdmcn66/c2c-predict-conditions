import pandas as pd
import os

# Créer une fonction pour calculer la moyenne sur une plage de colonnes
def calculer_moyenne(row, debut, fin, name_col):
    colonnes = [f'{name_col}{i}' for i in range(debut, fin+1)]
    return row[colonnes].mean()

def make_window_mean_data(df, name_col):
    dfcopy = df.copy()
    # Calculer les moyennes pour les plages spécifiées
    dfcopy['moyenne_' + name_col + '_jour_1_20'] = dfcopy.apply(lambda row: calculer_moyenne(row, 1, 20, name_col), axis=1)
    dfcopy['moyenne_' + name_col + '_jour_20_25'] = dfcopy.apply(lambda row: calculer_moyenne(row, 20, 25, name_col), axis=1)
    dfcopy['moyenne_' + name_col + '_jour_26_29'] = dfcopy.apply(lambda row: calculer_moyenne(row, 26, 29, name_col), axis=1)
    # Liste des colonnes originales
    colonnes_originales = [f'{name_col}{i}' for i in range(1, 30)]
    
    
    # Supprimer les colonnes originales
    dfcopy.drop(columns=colonnes_originales, inplace=True)
    return dfcopy


def build_standard_meteo():
    final_meteo = pd.read_csv('../data/intermediate/pivotted_meteo.csv')
    final_meteo = make_window_mean_data(final_meteo, 'daylight_duration_jour')
    final_meteo = make_window_mean_data(final_meteo, 'rain_sum_jour')
    final_meteo = make_window_mean_data(final_meteo, 'temperature_2m_min_jour')
    final_meteo = make_window_mean_data(final_meteo, 'temperature_2m_max_jour')
    final_meteo = make_window_mean_data(final_meteo, 'snowfall_sum_jour')

    if not os.path.isdir(os.path.join("./../data", "standard")):
        os.mkdir(os.path.join("./../data", "standard"))
    final_meteo.to_csv('../data/standard/meteo.csv',index = False)