import pandas as pd
import os


def build_datamart():
    final_outings = pd.read_csv('../data/standard/outings.csv')
    final_meteo = pd.read_csv('../data/standard/meteo.csv')
    datamart = final_meteo.merge(final_outings, how='inner', on='document_id')
    
    if not os.path.isdir(os.path.join("./../data", "datamart")):
        os.mkdir(os.path.join("./../data", "datamart"))
    datamart.to_csv('../data/datamart/outings_with_meteo.csv',index = False)
    