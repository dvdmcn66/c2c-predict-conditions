import pandas as pd
import os

def map_activity(row):
    if row['activity_snow_ice_mixed'] == True and row['activity_skitouring'] == True :
        return 'both'
    if row['activity_snow_ice_mixed'] == True:
        return 'alpinism'
    elif row['activity_skitouring'] == True:
        return 'ski'
    else:
        return None  # ou une autre valeur par défaut si nécessaire

def build_standard_outings():
    final_outings = pd.read_csv('../data/intermediate/filtered_columns_outings.csv')

    # Appliquer la fonction pour créer une nouvelle colonne "activity"
    final_outings['activity'] = final_outings.apply(lambda row: map_activity(row), axis=1)
    final_outings['date_start'] = pd.to_datetime(final_outings['date_start'])
    final_outings['mois'] = 'm' + final_outings['date_start'].dt.month.astype(str)

    final_outings.drop(columns=['activity_skitouring', 'activity_snow_ice_mixed', 'date_start'], inplace=True)

    if not os.path.isdir(os.path.join("./../data", "standard")):
        os.mkdir(os.path.join("./../data", "standard"))
    final_outings.to_csv('../data/standard/outings.csv',index = False)