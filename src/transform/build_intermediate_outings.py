import pandas as pd

def build_intermediate_outings():
    outings = pd.read_csv('../data/raw/outings.csv')
    intermediate_outings = outings.loc[:, ['document_id', 'date_start', 'elevation_max', 'snow_quality', 'condition_rating', 'activity_snow_ice_mixed', 'activity_skitouring']]
    intermediate_outings.to_csv('../data/intermediate/filtered_columns_outings.csv',index = False)