import pandas as pd
import os


def build_intermediate_meteo():
    data_meteo = pd.read_csv('../data/raw/meteo_sortie.csv')
    data_meteo = data_meteo.drop(columns=data_meteo.columns[data_meteo.columns.str.startswith('Unnamed:')])
    data_meteo = data_meteo.drop_duplicates(subset=['date', 'document_id'], keep='first')

    data_meteo.sort_values(by=['document_id', 'date'], inplace=True)
    grouped = data_meteo.groupby('document_id')

    # Ajouter une nouvelle colonne "jour" avec le compteur pour chaque groupe
    data_meteo['jour'] = grouped.cumcount() + 1

    # Renommer les dates par jour1, jour2, etc. pour chaque document_id
    data_meteo['jour'] = data_meteo['jour']
    data_meteo.drop(columns=['date'], inplace=True)

    index_to_drop = data_meteo.loc[data_meteo['jour'] > 31].index

    # Supprimer les lignes correspondantes
    data_meteo.drop(index=index_to_drop, inplace=True)
    data_meteo['jour'] = 'jour' + data_meteo['jour'].astype(str)

    pivot_data = data_meteo.pivot_table(index='document_id', columns='jour', aggfunc='first')
    pivot_data.columns =[s1 + '_' + str(s2) for (s1,s2) in pivot_data.columns.tolist()]
    pivot_data.reset_index(inplace=True)

    intermediate_meteo = pivot_data.copy()
    if not os.path.isdir(os.path.join("./../data", "intermediate")):
        os.mkdir(os.path.join("./../data", "intermediate"))
    intermediate_meteo.to_csv('../data/intermediate/pivotted_meteo.csv',index = False)