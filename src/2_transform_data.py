from transform.build_datamart import build_datamart
from transform.build_intermediate_meteo import build_intermediate_meteo
from transform.build_intermediate_outings import build_intermediate_outings
from transform.build_standard_meteo import build_standard_meteo
from transform.build_standard_outings import build_standard_outings

if __name__ == '__main__':
    build_intermediate_meteo()
    build_intermediate_outings()
    build_standard_meteo()
    build_standard_outings()
    build_datamart()
