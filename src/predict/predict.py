import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import OrdinalEncoder
from sklearn.metrics import r2_score
from sklearn.preprocessing import MinMaxScaler
from sklearn.decomposition import PCA
from sklearn.linear_model import LinearRegression, LogisticRegression
from sklearn.neural_network import MLPClassifier
from imblearn.over_sampling import SMOTE
from sklearn.metrics import confusion_matrix
import seaborn as sns
import matplotlib.pyplot as plt

def predict():
    scaler = MinMaxScaler()

    datamart = pd.read_csv('../data/datamart/outings_with_meteo.csv')
    datamart = datamart[datamart['activity'] == 'ski']
    # Supprimer les colonnes d'index et de document_id
    datamart = datamart.drop(columns=['document_id'])
    datamart_filtered = datamart.dropna(subset=['snow_quality', 'condition_rating'])

    snow_quality_order = ['awful', 'poor', 'average', 'good', 'excellent']
    condition_rating_order = ['awful', 'poor', 'average', 'good', 'excellent']

    # Créer l'encodeur en spécifiant l'ordre
    ordinal_encoder = OrdinalEncoder(categories=[snow_quality_order, condition_rating_order], handle_unknown='use_encoded_value', unknown_value=-1)

    # Appliquer l'encodage aux données
    datamart_filtered[['snow_quality', 'condition_rating']] = ordinal_encoder.fit_transform(datamart_filtered[['snow_quality', 'condition_rating']])
    # Diviser les données en ensembles d'entraînement et de test
    X = datamart_filtered.drop(columns=['snow_quality', 'condition_rating'])
    qualitative_data = X.select_dtypes(include=['object'])

    # Appliquer un encodage adapté aux données qualitatives
    # Par exemple, encodage one-hot
    encoded_qualitative_data = pd.get_dummies(qualitative_data)

    # Sélectionner les colonnes contenant des données quantitatives dans X
    quantitative_data = X.select_dtypes(exclude=['object'])

    # Concaténer les données qualitatives encodées et quantitatives
    X_processed = pd.concat([encoded_qualitative_data, quantitative_data], axis=1)
    X_processed.fillna(X_processed.median(), inplace=True)
    X_processed_normalized = scaler.fit_transform(X_processed)
    n_components = min(X_processed_normalized.shape[0], X_processed_normalized.shape[1])
    pca = PCA(n_components=n_components)

    # Appliquer PCA sur X_processed_normalized
    X_pca = pca.fit_transform(X_processed_normalized)

    # Sélectionner les 10 premières composantes principales
    X_PCA = X_pca[:, :10]

    y_snow_quality = datamart_filtered['snow_quality'].astype(int)
    y_condition_rating = datamart_filtered['condition_rating'].astype(int)


    resample=True
    if resample:
        sm_snow = SMOTE(random_state=42)
        X_processed_normalized_snow, y_snow_quality = sm_snow.fit_resample(X_processed_normalized, y_snow_quality)
        sm_cond = SMOTE(random_state=42)
        X_processed_normalized_cond, y_condition_rating = sm_cond.fit_resample(X_processed_normalized, y_condition_rating)
    else:
        X_processed_normalized_snow, y_snow_quality = X_processed_normalized, y_snow_quality
        X_processed_normalized_cond, y_condition_rating = X_processed_normalized, y_condition_rating

    X_train_snow, X_test_snow, y_snow_quality_train, y_snow_quality_test =  train_test_split(X_processed_normalized_snow, y_snow_quality, test_size=0.2, random_state=42)
    X_train_cond, X_test_cond, y_condition_rating_train, y_condition_rating_test = train_test_split(X_processed_normalized_cond, y_condition_rating, test_size=0.2, random_state=42)

    # Entraîner le modèle de régression logistique ordinale pour snow_quality
    model_snow_quality = MLPClassifier(random_state=1)
    model_snow_quality.fit(X_train_snow, y_snow_quality_train)

    # Entraîner le modèle de régression logistique ordinale pour condition_rating
    model_condition_rating = MLPClassifier(random_state=1)
    model_condition_rating.fit(X_train_cond, y_condition_rating_train)

    # Évaluer les performances du modèle pour snow_quality sur l'ensemble d'entraînement
    accuracy_snow_quality_train = model_snow_quality.score(X_train_snow, y_snow_quality_train)
    print("Précision du modèle pour snow_quality (entraînement) :", accuracy_snow_quality_train)
    accuracy_snow_quality = model_snow_quality.score(X_test_snow, y_snow_quality_test)
    print("Précision du modèle pour snow_quality :", accuracy_snow_quality)


    # Évaluer les performances du modèle pour condition_rating sur l'ensemble d'entraînement
    accuracy_condition_rating_train = model_condition_rating.score(X_train_cond, y_condition_rating_train)
    print("Précision du modèle pour condition_rating (entraînement) :", accuracy_condition_rating_train)
    # Évaluer les performances du modèle pour condition_rating
    accuracy_condition_rating = model_condition_rating.score(X_test_cond, y_condition_rating_test)
    print("Précision du modèle pour condition_rating :", accuracy_condition_rating)



    # Matrice de confusion pour snow_quality
    conf_matrix_snow_quality = confusion_matrix(y_snow_quality_test, model_snow_quality.predict(X_test_snow))

    # Matrice de confusion pour condition_rating
    conf_matrix_condition_rating = confusion_matrix(y_condition_rating_test, model_condition_rating.predict(X_test_cond))

    # Plot de la matrice de confusion pour snow_quality
    plt.figure(figsize=(8, 6))
    sns.heatmap(conf_matrix_snow_quality, annot=True, fmt='d', cmap='Blues', xticklabels=snow_quality_order, yticklabels=snow_quality_order)
    plt.xlabel('Prédictions')
    plt.ylabel('Valeurs réelles')
    plt.title('Matrice de confusion pour snow_quality')
    plt.show()

    # Plot de la matrice de confusion pour condition_rating
    plt.figure(figsize=(8, 6))
    sns.heatmap(conf_matrix_condition_rating, annot=True, fmt='d', cmap='Blues', xticklabels=condition_rating_order, yticklabels=condition_rating_order)
    plt.xlabel('Prédictions')
    plt.ylabel('Valeurs réelles')
    plt.title('Matrice de confusion pour condition_rating')
    plt.show()  